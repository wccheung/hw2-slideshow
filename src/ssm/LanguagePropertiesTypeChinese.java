/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm;

/**
 *
 * @author Computer Programming
 */
public enum LanguagePropertiesTypeChinese {
    // TITLE FOR WINDOW TITLE BAR
    // TITLE FOR WINDOW TITLE BAR
    标题窗口,
    
    // APPLICATION TOOLTIPS FOR BUTTONS
    TOOLTIP新幻灯片,
    TOOLTIP负载幻灯片,
    TOOLTIP保存幻灯片放映,
    TOOLTIP查看幻灯片放映,
    TOOLTIP出口,
    TOOLTIP添加幻灯片,
    TOOLTIP去除幻灯片,
    TOOLTIP提升,
    TOOLTIP下移,
    TOOLTIP前一张幻灯片,
    TOOLTIP下一张幻灯片,
    
    // DEFAULT VALUES
    默认的图片标题,
    默认的幻灯片标题,
    
    // UI LABELS
    标签标题,
      
    /* THESE ARE FOR LANGUAGE-DEPENDENT ERROR HANDLING,
     LIKE FOR TEXT PUT INTO DIALOG BOXES TO NOTIFY
     THE USER WHEN AN ERROR HAS OCCURED */
    在数据文件加载错误,
    错误的属性文件加载
}
